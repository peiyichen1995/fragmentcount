#pragma once

#include <iostream>
#include <iomanip>

class Node
{
public:
  Node(unsigned int id, float d, double x, double y) : _id(id), _d(d), _x(x), _y(y) {}
  unsigned int id() { return _id; }
  double x() { return _x; }
  double y() { return _y; }
  double d() { return _d; }
  void printNode()
  {
    std::cout << "exoid: " << _id << ", "
              << "coord: "
              << "(" << _x << "," << _y << ")" << std::endl;
  }

protected:
private:
  unsigned int _id;
  double _x;
  double _y;
  double _d;
};
