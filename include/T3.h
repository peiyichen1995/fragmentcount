#pragma once

#include "Node.h"
#include <bits/stdc++.h>
#include <map>

class T3
{
public:
  T3()
    : _id(0),
      _n1(NULL),
      _n2(NULL),
      _n3(NULL),
      _threshold(0),
      _fragment_id(0),
      _neighbors(NULL),
      _broken(true)
  {
  }
  T3(unsigned int id, Node * n1, Node * n2, Node * n3, double threshold)
    : _id(id), _n1(n1), _n2(n2), _n3(n3), _threshold(threshold), _fragment_id(0), _neighbors(NULL)
  {
    if (_n1->d() > _threshold || _n2->d() > _threshold || _n3->d() > _threshold)
      _broken = true;
    else
      _broken = false;
  }

  ~T3() {}

  unsigned int id() { return _id; }

  unsigned int fragment_id_elem() { return _fragment_id; }

  Node * n1() { return _n1; }

  Node * n2() { return _n2; }

  Node * n3() { return _n3; }

  bool broken() { return _broken; }

  void addNeighbor(T3 * neighbor) { _neighbors.push_back(neighbor); }

  void set_fragment_id(unsigned int id) { _fragment_id = id; }

  std::vector<double> get_x_coor()
  {
    std::vector<double> v;
    v.push_back(_n1->x());
    v.push_back(_n2->x());
    v.push_back(_n3->x());
    return v;
  };

  std::vector<double> get_y_coor()
  {
    std::vector<double> v;
    v.push_back(_n1->y());
    v.push_back(_n2->y());
    v.push_back(_n3->y());
    return v;
  };

  void printelement()
  {
    std::cout << "n1: ";
    _n1->printNode();
    std::cout << "n2: ";
    _n2->printNode();
    std::cout << "n3: ";
    _n3->printNode();
    std::cout << std::endl;
  }

  bool is_connected_to(T3 * e)
  {
    if (_n1 == e->n1())
      return true;
    if (_n1 == e->n2())
      return true;
    if (_n1 == e->n3())
      return true;
    if (_n2 == e->n1())
      return true;
    if (_n2 == e->n2())
      return true;
    if (_n2 == e->n3())
      return true;
    if (_n3 == e->n1())
      return true;
    if (_n3 == e->n3())
      return true;
    if (_n3 == e->n3())
      return true;
    return false;
  }

  std::vector<T3 *> getNeighbor() { return _neighbors; }

  double get_x_cen()
  {
    double ans = 0;
    ans += _n1->x();
    ans += _n2->x();
    ans += _n3->x();
    ans = ans / 3;
    return ans;
  }

  double get_y_cen()
  {
    double ans = 0;
    ans += _n1->y();
    ans += _n2->y();
    ans += _n3->y();
    ans = ans / 3;
    return ans;
  }

protected:
private:
  unsigned int _id;
  Node * _n1;
  Node * _n2;
  Node * _n3;
  bool _broken;
  double _threshold;
  std::vector<T3 *> _neighbors;
  unsigned int _fragment_id;
};
