#pragma once

#include <cstdlib>
#include <cmath>

inline void
Assert(bool assertion)
{
  if (!assertion)
  {
    std::cout << "Assertion failed.\n";
    exit(1);
  }
}

template <typename N>
inline void
AssertAbsoluteFuzzyEqual(N a, N b, N tol = 1e-6)
{
  if (std::abs(a - b) > tol)
  {
    std::cout << "Assertion failed.\n";
    exit(1);
  }
}
