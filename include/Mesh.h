#pragma once
#include "exodusII.h"
#include "Node.h"
#include <iostream>
#include <vector>
#include <map>
#include "T3.h"
#include "matplotlibcpp.h"
#include "Fragment.h"
#include <algorithm>

class Mesh
{
public:
  Mesh(char const * file, int CPU_word_size, int IO_word_size, double threshold)
    : _CPU_word_size(CPU_word_size), _IO_word_size(IO_word_size), _threshold(threshold)
  {
    _title = (char *)calloc(MAX_LINE_LENGTH + 1, sizeof(char));
    _exoid = ex_open(file, EX_READ, &_CPU_word_size, &_IO_word_size, &_version);
    ex_inquire(_exoid, EX_INQ_API_VERS, &_idum, &_version, _cdum);
    _error["ex_get_init"] = ex_get_init(_exoid,
                                        _title,
                                        &_num_dim,
                                        &_num_nodes,
                                        &_num_elem,
                                        &_num_elem_blk,
                                        &_num_node_sets,
                                        &_num_side_sets);

    float *x, *y, *z;

    x = (float *)calloc(_num_nodes, sizeof(float));
    if (_num_dim >= 2)
      y = (float *)calloc(_num_nodes, sizeof(float));
    else
      y = 0;
    if (_num_dim >= 3)
      z = (float *)calloc(_num_nodes, sizeof(float));
    else
      z = 0;

    _error["ex_get_coord"] = ex_get_coord(_exoid, x, y, z);

    /* read a nodal variable at one time step */

    _var_values = (float *)calloc(_num_nodes, sizeof(float));

    _time_step = 111;
    _var_index = 1;

    _error["ex_get_var"] =
        ex_get_var(_exoid, _time_step, EX_NODAL, _var_index, 1, _num_nodes, _var_values);

    // std::cout << "nodal variable " << _var_index << " values at time step" << _time_step;
    // for (int i = 0; i < _num_nodes; i++)
    //   std::cout << _var_values[i] << std::endl;

    /* Create new nodes */

    for (int i = 0; i < _num_nodes; i++)
      _nodes.push_back(new Node(i + 1, _var_values[i], x[i], y[i]));

    std::vector<float> xc;
    std::vector<float> yc;
    std::vector<float> v;

    for (int i = 0; i < _num_nodes; i++)
    {
      xc.push_back(x[i]);
      yc.push_back(y[i]);
      v.push_back(_var_values[i]);
    }
    // draw scatters of elements
    // matplotlibcpp::backend("tkAgg");
    // matplotlibcpp::scatter(xc, yc, 1.0, v, "jet");
    // matplotlibcpp::show();

    free(_var_values);
    free(x);
    free(y);
    free(z);

    _ids = (int *)calloc(_num_elem_blk, sizeof(int));
    _num_elem_in_block = (int *)calloc(_num_elem_blk, sizeof(int));
    _num_nodes_per_elem = (int *)calloc(_num_elem_blk, sizeof(int));
    _num_attr = (int *)calloc(_num_elem_blk, sizeof(int));

    _error["ex_get_ids"] = ex_get_ids(_exoid, EX_ELEM_BLOCK, _ids);

    _elem_type = (char *)calloc(MAX_STR_LENGTH + 1, sizeof(char));

    for (unsigned int i = 0; i < _num_elem_blk; i++)
    {
      _error["ex_get_block"] = ex_get_block(_exoid,
                                            EX_ELEM_BLOCK,
                                            _ids[i],
                                            _elem_type,
                                            &(_num_elem_in_block[i]),
                                            &(_num_nodes_per_elem[i]),
                                            NULL,
                                            NULL,
                                            &(_num_attr[i]));

      if (_error["ex_get_block"])
        break;
    }

    // _connect = (int *)calloc((_num_nodes_per_elem[0] * _num_elem_in_block[0]), sizeof(int));
    // _error["ex_get_1_elem_conn"] =
    //     ex_get_conn(_exoid, EX_ELEM_BLOCK, _ids[0], _connect, NULL, NULL);
    //
    // std::cout << "node list for first element of element block: " << _ids[0] << std::endl;
    // for (unsigned int j = 0; j < _num_nodes_per_elem[0]; j++)
    // {
    //   std::cout << _connect[j] << " ";
    // }
    // std::cout << std::endl;

    for (unsigned int i = 0; i < _num_elem_blk; i++)
    {
      _connect = (int *)calloc((_num_nodes_per_elem[i] * _num_elem_in_block[i]), sizeof(int));

      _error["ex_get_conn"] = ex_get_conn(_exoid, EX_ELEM_BLOCK, _ids[i], _connect, NULL, NULL);
      for (unsigned int j = 0; j < _num_elem_in_block[i]; j++)
      {
        _elements.push_back(new T3(j + 1,
                                   _nodes[_connect[j * _num_nodes_per_elem[i]] - 1],
                                   _nodes[_connect[j * _num_nodes_per_elem[i] + 1] - 1],
                                   _nodes[_connect[j * _num_nodes_per_elem[i] + 2] - 1],
                                   _threshold));

        // std::cout << "elem number = " << j << std::endl;
        // std::cout << "n1 id = " << _nodes[_connect[j] - 1]->id() << std::endl;
        // std::cout << "n2 id = " << _nodes[_connect[j + 1] - 1]->id() << std::endl;
        // std::cout << "n3 id = " << _nodes[_connect[j + 2] - 1]->id() << std::endl;
      }

      free(_connect);
    }

    /* read global variables parameters and names */

    _error["ex_inquire"] = ex_inquire(_exoid, EX_INQ_TIME, &_num_time_steps, &_fdum, _cdum);

    _error["ex_get_variable_param_glo"] = ex_get_variable_param(_exoid, EX_GLOBAL, &_num_glo_vars);

    for (unsigned int i = 0; i < _num_glo_vars; i++)
      _var_names_glo[i] = (char *)calloc((MAX_STR_LENGTH + 1), sizeof(char));

    _error["ex_get_variable_names_glo"] =
        ex_get_variable_names(_exoid, EX_GLOBAL, _num_glo_vars, _var_names_glo);

    std::cout << "There are " << _num_glo_vars
              << " global variables; their names are: " << std::endl;
    for (unsigned int i = 0; i < _num_glo_vars; i++)
      std::cout << _var_names_glo[i] << std::endl;

    /* read nodal variables parameters and names */

    _error["ex_get_variable_param_nod"] = ex_get_variable_param(_exoid, EX_NODAL, &_num_nod_vars);

    for (unsigned int i = 0; i < _num_nod_vars; i++)
      _var_names_nod[i] = (char *)calloc((MAX_STR_LENGTH + 1), sizeof(char));

    _error["ex_get_variable_names_nod"] =
        ex_get_variable_names(_exoid, EX_NODAL, _num_nod_vars, _var_names_nod);

    std::cout << "There are " << _num_nod_vars
              << " nodal variables; their names are: " << std::endl;
    for (unsigned int i = 0; i < _num_nod_vars; i++)
      std::cout << _var_names_nod[i] << std::endl;

    findNeighbor();
    std::cout << "Find neighbors finished." << std::endl;
    std::cout << "Find fragments started..." << std::endl;
    findFragment();
    // draw_fragments_fill();
    melt_broken();
    draw_fragments_cen_scatter();
  }

  ~Mesh()
  {
    free(_title);
    free(_elem_type);
    free(_ids);
    free(_num_elem_in_block);
    free(_num_nodes_per_elem);
    free(_num_attr);
    // free(_var_names_glo);
    // free(_var_names_nod);
    for (unsigned int i = 0; i < _num_elem; i++)
      delete _elements[i];
    for (unsigned int i = 0; i < _num_nodes; i++)
      delete _nodes[i];
    for (unsigned int i = 0; i < _num_glo_vars; i++)
      free(_var_names_glo[i]);
    for (unsigned int i = 0; i < _num_nod_vars; i++)
      free(_var_names_nod[i]);
  }

  int getExoid()
  {
    std::cout << "exoid: " << _exoid << std::endl;
    return _exoid;
  }

  float getVersion()
  {
    std::cout << "version: " << _version << std::endl;
    return _version;
  }

  std::map<std::string, int> getError() { return _error; }

  char getTitle()
  {
    std::cout << "title: " << _title << std::endl;
    return *_title;
  }

  char * getelem_type()
  {
    std::cout << "elem type: " << _elem_type << std::endl;
    return _elem_type;
  }

  int getnum_dim()
  {
    std::cout << "num_dim: " << _num_dim << std::endl;
    return _num_dim;
  }

  int getnum_nodes()
  {
    std::cout << "num_nodes: " << _num_nodes << std::endl;
    return _num_nodes;
  }

  int getnum_elem()
  {
    std::cout << "num_elem: " << _num_elem << std::endl;
    return _num_elem;
  }

  void findNeighbor()
  {
    for (unsigned int i = 0; i < _num_elem - 1; i++)
    {
      for (unsigned int j = i + 1; j < _num_elem; j++)
      {
        if (_elements[i]->is_connected_to(_elements[j]))
        {
          _elements[i]->addNeighbor(_elements[j]);
          _elements[j]->addNeighbor(_elements[i]);
        }
      }
    }
  }

  void findFragment()
  {
    std::vector<T3 *> tmp_elements = _elements;
    std::vector<T3 *> queue;
    std::vector<T3 *> tmp_neighbor;
    Fragment * tmp;
    unsigned int fragment_id = 1;
    while (tmp_elements.size() != 0)
    {
      std::cout << "tmp_elements.size() = " << tmp_elements.size() << std::endl;
      if (tmp_elements[0]->broken())
      {
        _broken->add(tmp_elements[0]);
        tmp_elements.erase(tmp_elements.begin());
      }
      else
      {
        tmp = new Fragment(fragment_id);
        queue.push_back(tmp_elements[0]);
        tmp_elements.erase(tmp_elements.begin());
        while (queue.size() != 0)
        {
          // std::cout << "queue.size() = " << queue.size() << std::endl;
          // std::cout << "queue[0] id: " << queue[0]->id() << std::endl;
          if (queue[0]->broken())
          {
            _broken->add(queue[0]);
            queue.erase(queue.begin());
          }
          else
          {
            tmp_neighbor = queue[0]->getNeighbor();
            queue[0]->set_fragment_id(fragment_id);
            tmp->add(queue[0]);
            queue.erase(queue.begin());
            // std::cout << "tmp_neighbor size: " << tmp_neighbor.size() << std::endl;
            for (unsigned int i = 0; i < tmp_neighbor.size(); i++)
            {
              if (tmp->find(tmp_neighbor[i]) || _broken->find(tmp_neighbor[i]) ||
                  std::find(queue.begin(), queue.end(), tmp_neighbor[i]) != queue.end())
                continue;
              else
              {
                if (tmp_neighbor[i]->broken())
                  _broken->add(tmp_neighbor[i]);
                else
                  queue.push_back(tmp_neighbor[i]);
                auto index = std::find(tmp_elements.begin(), tmp_elements.end(), tmp_neighbor[i]);
                tmp_elements.erase(index);
              }
            }
          }
        }
        _fragments.push_back(tmp);
        std::cout << "fragment id: " << _fragments[_fragments.size() - 1]->fragment_id()
                  << std::endl;
        std::cout << "fragment size: " << tmp->size() << std::endl;
        std::cout << "_broken size: " << _broken->size() << std::endl;
        fragment_id++;
      }
    }
  }

  void draw_fragments_fill()
  {
    matplotlibcpp::backend("tkAgg");
    std::vector<double> xf;
    std::vector<double> yf;
    std::vector<T3 *> tmp_elements;
    std::map<std::string, std::string> keywords;
    keywords["color"] = "blue";

    for (int i = 0; i < _fragments.size(); i++)
    {
      tmp_elements = _fragments[i]->get_elements();
      for (unsigned int k = 0; k < tmp_elements.size(); k++)
      {
        xf = tmp_elements[k]->get_x_coor();
        yf = tmp_elements[k]->get_y_coor();
        matplotlibcpp::fill(xf, yf, keywords);
      }
      std::cout << "plotted fragment " << i << std::endl;
    }
    matplotlibcpp::show();
    for (unsigned int i = 0; i < tmp_elements.size(); i++)
      delete tmp_elements[i];
  }

  void draw_fragments_cen_scatter()
  {
    matplotlibcpp::backend("tkAgg");
    std::vector<double> xc;
    std::vector<double> yc;
    std::vector<double> id;
    double v;
    std::vector<T3 *> tmp_elements;
    for (int i = 0; i < _fragments.size(); i++)
    {
      tmp_elements = _fragments[i]->get_elements();
      v = _fragments[i]->id();
      for (unsigned int k = 0; k < tmp_elements.size(); k++)
      {
        xc.push_back(tmp_elements[k]->get_x_cen());
        yc.push_back(tmp_elements[k]->get_y_cen());
        id.push_back(double(v));
      }
    }
    matplotlibcpp::scatter(xc, yc, 1.0, id, "jet");
    matplotlibcpp::show();
    for (unsigned int i = 0; i < tmp_elements.size(); i++)
      delete tmp_elements[i];
  }

  void melt_broken()
  {
    for (unsigned int i = 0; i < _elements.size(); i++)
      std::cout << "elem id " << _elements[i]->id()
                << ", fragement id: " << _elements[i]->fragment_id_elem() << std::endl;
    for (unsigned int i = 0; i < _fragments.size(); i++)
    {
      std::cout << "finding fragment " << i << std::endl;
      _fragments[i]->find_boundary();
    }

    std::vector<T3 *> brokens = _broken->get_elements();
    unsigned int index;    // fragment_id
    unsigned int location; // fragment location in _fragments
    double distance;
    double tmp_dis;
    double x, y;
    for (unsigned int i = 0; i < brokens.size(); i++)
    {
      x = brokens[i]->get_x_cen();
      y = brokens[i]->get_y_cen();
      distance = _fragments[0]->get_shortest_dis(x, y);
      index = _fragments[0]->id();
      for (unsigned int j = 1; j < _fragments.size(); j++)
      {
        tmp_dis = _fragments[j]->get_shortest_dis(x, y);
        if (distance > tmp_dis)
        {
          distance = tmp_dis;
          index = _fragments[j]->id();
        }
      }
      location = find_loc_with_id(index);
      _fragments[location]->add(brokens[i]);
    }
  }

  unsigned int find_loc_with_id(unsigned int id)
  {
    for (unsigned int i = 0; i < _fragments.size(); i++)
    {
      if (_fragments[i]->id() == id)
        return i;
    }
    return 0;
  }

  std::vector<Node *> getnodes() { return _nodes; }

  std::vector<T3 *> getelements() { return _elements; }

  std::vector<Fragment *> get_fragments() { return _fragments; }

protected:
private:
  float _version;
  int _CPU_word_size;
  int _IO_word_size;
  int _exoid;
  std::map<std::string, int> _error;
  char * _title;
  int _num_dim, _num_nodes, _num_elem, _num_elem_blk, _num_node_sets, _num_side_sets;
  std::vector<Node *> _nodes;
  std::vector<T3 *> _elements;
  char * _elem_type;
  int * _ids;
  int * _num_elem_in_block;
  int * _num_nodes_per_elem;
  int * _num_attr;
  int * _connect;
  double _threshold;
  int _num_time_steps;
  float _fdum;
  char * _cdum = 0;
  int _idum;
  int _num_glo_vars, _num_nod_vars, _num_ele_vars;
  char * _var_names_glo[3];
  char * _var_names_nod[3];
  float * _var_values;
  int _time_step, _var_index;
  Fragment * _broken = new Fragment(0);
  std::vector<Fragment *> _fragments;
};
