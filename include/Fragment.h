#pragma once
#include "Node.h"
#include "T3.h"
#include <vector>
#include <iostream>
#include <math.h>

class Fragment
{
public:
  Fragment() : _fragment_id(0), _elements(NULL), _boundary(NULL) {}

  Fragment(unsigned int id) : _fragment_id(id), _elements(NULL), _boundary(NULL) {}

  void add(T3 * e) { _elements.push_back(e); }

  unsigned int fragment_id() { return _fragment_id; }

  unsigned int size() { return _elements.size(); }

  unsigned int id() { return _fragment_id; }

  bool find(T3 * e)
  {
    for (unsigned int i = 0; i < _elements.size(); i++)
    {
      if (_elements[i] == e)
        return true;
    }
    return false;
  }

  void find_boundary()
  {
    std::vector<T3 *> neighbors;
    for (unsigned int i = 0; i < _elements.size(); i++)
    {
      neighbors = _elements[i]->getNeighbor();
      for (unsigned int j = 0; j < neighbors.size(); j++)
      {
        if (neighbors[j]->fragment_id_elem() != _fragment_id)
        {
          _boundary.push_back(_elements[i]);
          break;
        }
      }
    }
  }

  double get_shortest_dis(double x, double y)
  {
    double distance;

    distance = sqrt(pow(_boundary[0]->get_x_cen() - x, 2) + pow(_boundary[0]->get_y_cen() - y, 2));

    for (unsigned int i = 1; i < _boundary.size(); i++)
    {
      if (distance >
          sqrt(pow(_boundary[i]->get_x_cen() - x, 2) + pow(_boundary[i]->get_y_cen() - y, 2)))
        distance =
            sqrt(pow(_boundary[i]->get_x_cen() - x, 2) + pow(_boundary[i]->get_y_cen() - y, 2));
    }

    return distance;
  }

  std::vector<T3 *> get_boundary() { return _boundary; }

  std::vector<T3 *> get_elements() { return _elements; }

protected:
private:
  std::vector<T3 *> _elements;
  unsigned int _fragment_id;
  std::vector<T3 *> _boundary;
};
