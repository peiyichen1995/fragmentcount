CXXFLAGS = -Wall -g -Iinclude -I/opt/moose/seacas/include -I/usr/include/python2.7
CXX = g++
LDLIBS = -lexodus -lpython2.7
LDFLAGS = -L/opt/moose/seacas/lib -L/usr/lib/python2.7

src = $(wildcard src/*.cpp)
obj = $(src:.cpp=.o)

FragmentCount: $(obj)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)

.PHONY: clean

clean:
	rm -f src/*.o
