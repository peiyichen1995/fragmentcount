// FragmentCount includes
#include "T3.h"
#include "Assert.h"

// std includes
#include <iostream>

void
test_Node()
{
  std::cout << "======================================\n";
  std::cout << "In " << __FUNCTION__ << std::endl;
  Node * n = new Node(5, 1.0, 2.2);
  Assert(n->id() == 5);
  AssertAbsoluteFuzzyEqual(n->x(), 1.0);
  AssertAbsoluteFuzzyEqual(n->y(), 2.2);
  std::cout << "Test succeedded!" << std::endl;
}

int
main(int argc, char const * argv[])
{
  test_Node();
  return 0;
}
