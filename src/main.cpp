#include "exodusII.h"
#include <iostream>
#include <map>
#include "Mesh.h"
#include "Node.h"

int
main(int argc, char const * argv[])
{

  int CPU_word_size = 0;
  int IO_word_size = 4;
  double threshold = 0.7;

  // int exoid = ex_open(argv[1], EX_READ, &CPU_word_size, &IO_word_size, &version);
  //
  // std::cout << "exoid: " << exoid << std::endl;
  // std::cout << "CPU_word_size: " << CPU_word_size << std::endl;
  // std::cout << "IO_word_size: " << IO_word_size << std::endl;
  // std::cout << "version: " << version << std::endl;

  Mesh A(argv[1], CPU_word_size, IO_word_size, threshold);
  int exoid = A.getExoid();
  float version = A.getVersion();
  std::cout << "CPU_word_size: " << CPU_word_size << std::endl;
  std::cout << "IO_word_size: " << IO_word_size << std::endl;
  std::cout << "version: " << version << std::endl;

  //////////////////////////////////////////////////////////////////

  // char title[MAX_LINE_LENGTH + 1];

  // int num_dim, num_nodes, num_elem, num_elem_blk, num_node_sets, num_side_sets;
  // int error = ex_get_init(
  //     exoid, title, &num_dim, &num_nodes, &num_elem, &num_elem_blk, &num_node_sets,
  //     &num_side_sets);

  std::map<std::string, int> error = A.getError();

  if (error["ex_get_init"])
  {
    std::cout << "ex_get_init failed." << std::endl;
    return 1;
  }

  // std::cout << "title: " << title << std::endl;
  // std::cout << "num_dim: " << num_dim << std::endl;
  // std::cout << "num_nodes: " << num_nodes << std::endl;
  // std::cout << "num_elem: " << num_elem << std::endl;

  char title = A.getTitle();
  int num_dim = A.getnum_dim();
  int num_nodes = A.getnum_nodes();
  int num_elem = A.getnum_elem();

  // float *x, *y, *z;
  //
  // x = (float *)calloc(num_nodes, sizeof(float));
  // if (num_dim >= 2)
  //   y = (float *)calloc(num_nodes, sizeof(float));
  // else
  //   y = 0;
  //
  // if (num_dim >= 3)
  //   z = (float *)calloc(num_nodes, sizeof(float));
  // else
  //   z = 0;
  //
  // error = ex_get_coord(exoid, x, y, z);
  //
  if (error["ex_get_coord"])
  {
    std::cout << "ex_get_coord failed." << std::endl;
    return 1;
  }

  std::vector<Node *> nodes = A.getnodes();

  std::cout << "1st node: " << std::endl;
  nodes[0]->printNode();

  //
  // char elem_type[MAX_STR_LENGTH + 1];
  //
  // int * ids = (int *)calloc(num_elem_blk, sizeof(int));
  // int * num_elem_in_block = (int *)calloc(num_elem_blk, sizeof(int));
  // int * num_nodes_per_elem = (int *)calloc(num_elem_blk, sizeof(int));
  // int * num_attr = (int *)calloc(num_elem_blk, sizeof(int));
  //
  // error = ex_get_ids(exoid, EX_ELEM_BLOCK, ids);
  //
  if (error["ex_get_ids"])
  {
    std::cout << "ex_get_ids failed." << std::endl;
    return 1;
  }
  //
  // for (int i = 0; i < num_elem_blk; i++)
  // {
  //   error = ex_get_block(exoid,
  //                        EX_ELEM_BLOCK,
  //                        ids[i],
  //                        elem_type,
  //                        &(num_elem_in_block[i]),
  //                        &(num_nodes_per_elem[i]),
  //                        NULL,
  //                        NULL,
  //                        &(num_attr[i]));
  //
  //   if (error)
  //   {
  //     std::cout << "ex_get_block failed." << std::endl;
  //     return 1;
  //   }

  if (error["ex_get_block"])
  {
    std::cout << "ex_get_block failed." << std::endl;
    return 1;
  }

  char * elem_type;
  elem_type = A.getelem_type();

  std::vector<T3 *> elements = A.getelements();

  //
  //   std::cout << "=====================================" << std::endl;
  //
  //   std::cout << "Element block id: " << ids[i] << std::endl;
  //   std::cout << "Element type: " << elem_type << std::endl;
  //   std::cout << "NUmber of elements in block" << ids[i] << ": " << num_elem_in_block[i]
  //             << std::endl;
  //   std::cout << "NUmber of nodes per element: " << num_nodes_per_elem[i] << std::endl;
  //   std::cout << "NUmber of attributes: " << num_attr[i] << std::endl;
  // }

  std::cout << "=====================================" << std::endl;

  // std::cout << nodes.size() << num_nodes;

  // std::cout << "1st element connectivity: " << std::endl;
  // elements[0]->printelement();
  // std::cout << std::endl;

  // for (unsigned int i = 0; i < num_elem; i++)
  //   elements[i]->printelement();
  //
  // for (unsigned int i = 0; i < num_nodes; i++)
  //   nodes[i]->printNode();

  return 0;
}
